import 'package:f_app/services/auth.dart';
import 'package:f_app/ui/home/feed.dart';
import 'package:f_app/ui/profile/userProfile.dart';

import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.auth, this.onSignedOut, this.user}) : super(key: key);

  final Auth auth;
  final VoidCallback onSignedOut;
  final Map<String, dynamic> user;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;


  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> _children = [
      Feed(
        auth: widget.auth,
        onSignedOut: widget.onSignedOut,
      ),
      UserProfile(
          auth: widget.auth, onSignedOut: widget.onSignedOut, user: widget.user)
    ];

    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped, // new
        currentIndex: _currentIndex, //
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.home, color: Colors.grey),
              activeIcon: Icon(FontAwesomeIcons.home, color: Colors.black),
              title: new Text('')),
          BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.user, color: Colors.grey),
              activeIcon: Icon(FontAwesomeIcons.user, color: Colors.black),
              title: new Text(''))
        ],
      ),
    );
  }
}

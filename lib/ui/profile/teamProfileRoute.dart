import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:location/location.dart';

class TeamProfileRoute extends StatefulWidget {
  TeamProfileRoute(this.teamId);
  final String teamId;

  @override
  _TeamProfileRouteState createState() => _TeamProfileRouteState();
}

class _TeamProfileRouteState extends State<TeamProfileRoute> {
  var currentLocation = LocationData;

  Location location = new Location();

  Firestore firestore = Firestore.instance;
  Geoflutterfire geo = Geoflutterfire();

  _addGeoPoint() async {
  var pos = await location.getLocation();
  
  GeoFirePoint point = geo.point(latitude: pos.latitude, longitude: pos.longitude);
  firestore.collection('teams').document(widget.teamId).updateData({ 
    'geoPosition': point.data,
  });
}



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream:
            Firestore.instance.collection('teams').document(widget.teamId).snapshots(),
        builder:
            (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            DocumentSnapshot teamDocument = snapshot.data;
            return CustomScrollView(
              slivers: <Widget>[
                _sliverHeaderTeam(teamDocument),
                _sliverTeamsInfo(teamDocument),
                _sliverPlayersTitle(teamDocument),
                _sliverListPlayers(teamDocument),
              ],
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        elevation: 0.0,
        onPressed: _addGeoPoint,
        icon: Icon(Icons.send),
        label: Text("Solicitar desafío"),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(12.0),
          ),
        ),
        backgroundColor: Colors.orangeAccent,
      ),
    );
  }

  Widget _sliverHeaderTeam(DocumentSnapshot teamDocument) {
    return SliverAppBar(
      brightness: Brightness.dark,
      iconTheme: IconThemeData(color: Colors.white),
      elevation: 1.0,
      pinned: true,
      expandedHeight: 250.0,
      backgroundColor: Colors.deepOrangeAccent,
      flexibleSpace: FlexibleSpaceBar(
        title: Text(
          teamDocument['name'],
          style: TextStyle(color: Colors.white),
        ),
        background: Image.network(teamDocument['photoUrl']),
      ),
    );
  }

  Widget _sliverTeamsInfo(teamDocument) {
    
    GeoPoint geoPoint = teamDocument['geoPosition']['geopoint'];
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text(
                "Jugadores: ${teamDocument['totalPlayers'].toString()}",
                style: TextStyle(fontSize: 16),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text(
                "Promedio edad: ${teamDocument['middleAge'].toString()}",
                style: TextStyle(fontSize: 16),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 8),
              child: Text(
                "Geo ubicación: ${geoPoint.latitude.toString()}, ${geoPoint.longitude.toString()}",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _sliverPlayersTitle(teamDocument) {
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.only(left: 16, bottom: 12),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Jugadores",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
                IconButton(
                  icon: Icon(Icons.keyboard_arrow_down),
                  highlightColor: Color.fromRGBO(250, 255, 255, 1),
                  onPressed: () => {},
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _sliverListPlayers(teamDocument) {
    List<dynamic> map = teamDocument['players'];
    return SliverFixedExtentList(
      itemExtent: 55.0,
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        return Container(
          alignment: Alignment.center,
          child: ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(map.toList()[index]['photoUrl']),
              ),
              title: Text(map.toList()[index]['displayName']),
              subtitle: Text(map.toList()[index]['position'])),
        );
      }, childCount: map.length),
    );
  }
}

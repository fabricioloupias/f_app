import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:f_app/services/auth.dart';

import 'package:flutter/material.dart';

class UserProfile extends StatefulWidget {
  UserProfile({
    Key key,
    this.auth,
    this.onSignedOut,
    this.user
  }) : super(key: key);

  final Auth auth;
  final VoidCallback onSignedOut;
  final Map<String, dynamic> user;

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  
  void _signOut() async {
    try {
      widget.auth.signOut();
      widget.onSignedOut();
    } catch (e) {}
  }

  


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<DocumentSnapshot>(
        stream:
            Firestore.instance.collection('users').document(widget.user['uid'].toString()).snapshots(),
        builder:
            (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            var userDocument = snapshot.data;
            return CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  elevation: 1,
                  pinned: true,
                  floating: false,
                  snap: false,
                  expandedHeight: 250.0,
                  flexibleSpace: FlexibleSpaceBar(
                    title: Text('Perfil'),
                    centerTitle: true,
                  ),
                  actions: <Widget>[
                    PopupMenuButton<int>(
                      itemBuilder: (context) => [
                            PopupMenuItem(
                              value: 1,
                              child: Text("Cerrar sesión"),
                            ),
                          ],
                      onSelected: (value) {
                        switch (value) {
                          case 1:
                            _signOut();
                            break;
                          default:
                        }
                      },
                    ),
                  ],
                ),
                _perfilHeader(userDocument),
                _sliverPerfilTitle(),
                _sliverPerfilBody(userDocument),
              ],
            );
          }
        },
      ),
    );
  }

  Widget _perfilHeader(DocumentSnapshot userDocument) {
    return SliverToBoxAdapter(
      child: Container(
        margin: EdgeInsets.only(top: 50, left: 16, right: 16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: Text(
                    userDocument["displayName"],
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    userDocument["position"],
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
              ],
            ),
            Container(
              width: 90.0,
              height: 90.0,
              decoration: new BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 5.0,
                  ),
                ],
                shape: BoxShape.circle,
                image: new DecorationImage(
                  fit: BoxFit.fill,
                  image: new NetworkImage(userDocument["photoUrl"]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _sliverPerfilTitle() {
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.only(left: 16, top: 20, bottom: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Participas en:",
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
            ),
            IconButton(
              icon: Icon(Icons.keyboard_arrow_down),
              highlightColor: Color.fromRGBO(250, 255, 255, 1),
              onPressed: () => {},
            )
          ],
        ),
      ),
    );
  }

  Widget _sliverPerfilBody(DocumentSnapshot userDocument) {
    List<dynamic> map = userDocument["teams"];
    return SliverFixedExtentList(
      itemExtent: 310.0,
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return StreamBuilder<DocumentSnapshot>(
            stream: Firestore.instance
                .collection('teams')
                .document(map.toList()[index]['idTeam'])
                .snapshots(),
            builder: (BuildContext context,
                AsyncSnapshot<DocumentSnapshot> snapshot) {
              DocumentSnapshot ds = snapshot.data;
              if (snapshot.hasData) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Image.network(
                        ds.data['photoUrl'].toString(),
                        fit: BoxFit.cover,
                      ),
                      padding: EdgeInsets.only(left: 16, right: 16),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      padding: EdgeInsets.only(left: 16),
                      child: Text(ds.data['name'].toString()),
                    ),
                  ],
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          );
        },
        childCount: map.length,
      ),
    );
  }
}

/*
Widget getUserData(String id) {
  return new StreamBuilder(
      stream: Firestore.instance.collection('users').document(id).snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return new Text("Loading");
        }
        var userDocument = snapshot.data;
        return Container(
          margin: EdgeInsets.only(top: 50.0),
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(userDocument["displayName"],
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                  ),
                  Text(userDocument["email"],
                    style: TextStyle(fontSize: 14,),
                  ),
                ],
              ),
              CircleAvatar(
                radius: 40,
                backgroundImage: NetworkImage(userDocument["photoUrl"]),
              )
              ],
          ),
        );
      }
  );
}


*/

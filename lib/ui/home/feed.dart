import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:f_app/services/auth.dart';
import 'package:f_app/ui/profile/teamProfileRoute.dart';
import 'package:flutter/material.dart';

class Feed extends StatefulWidget {
  Feed({
    this.auth,
    this.onSignedOut,
  });
  final BaseAuth auth;
  final VoidCallback onSignedOut;

  @override
  _FeedState createState() => _FeedState();
}

class _FeedState extends State<Feed> {
  int totalTeams;

  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<QuerySnapshot>(
          stream: Firestore.instance.collection('teams').snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              return mostraEquipos(snapshot);
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }

  Widget mostraEquipos(AsyncSnapshot<QuerySnapshot> snapshot) {
    return CustomScrollView(
      slivers: <Widget>[
        _sliverAppBar(),
        _sliverTeamsNear(snapshot),
        _sliverTitleRecomended(),
        _sliverRecomended(snapshot),
      ],
    );
  }

  Widget _sliverAppBar() {
    return SliverAppBar(
      elevation: 1,
      pinned: true,
      floating: false,
      snap: false,
      expandedHeight: 250.0,
      flexibleSpace: FlexibleSpaceBar(
        title: Text('Home'),
        centerTitle: true,
      ),
    );
  }

  Widget _sliverTeamsNear(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<dynamic> map = snapshot.data.documents;
    return SliverToBoxAdapter(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 16, top: 12, bottom: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Cerca tuyo",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
                IconButton(
                  icon: Icon(Icons.keyboard_arrow_down),
                  onPressed: () => {},
                )
              ],
            ),
          ),
          Container(
            height: 220,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: map.toList().length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => TeamProfileRoute(map.toList()[index]["teamId"])),
                          );
                        },
                        child: Container(
                          height: 180,
                          child: Image.network(
                            map.toList()[index]["photoUrl"],
                            fit: BoxFit.cover,
                          ),
                          padding: EdgeInsets.only(left: 16),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 10),
                            padding: EdgeInsets.only(left: 16),
                            child: Text('${map.toList()[index]['name']} · '),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 10),
                            child: Text('${map.toList()[index]['barrio']}',
                              style: TextStyle(
                                fontWeight: FontWeight.w500
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  );
                }),
          )
        ],
      ),
    );
  }

  Widget _sliverTitleRecomended() {
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.only(left: 16, top: 12, bottom: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Recomendados",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
            ),
            IconButton(
              icon: Icon(Icons.keyboard_arrow_down),
              highlightColor: Color.fromRGBO(250, 255, 255, 1),
              onPressed: () => {},
            )
          ],
        ),
      ),
    );
  }

  Widget _sliverRecomended(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<dynamic> map = snapshot.data.documents;
    return SliverFixedExtentList(
      itemExtent: 330.0,
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TeamProfileRoute(map.toList()[index]["teamId"])),
                  );
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Image.network(
                    map.toList()[index]["photoUrl"],
                    fit: BoxFit.cover,
                  ),
                  padding: EdgeInsets.only(left: 16, right: 16),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.only(left: 16),
                child: Text(map.toList()[index]['name']),
              ),
            ],
          );
        },
        childCount: map.length,
      ),
    );
  }
}

/*
Widget mostarEquipos(){
  return StreamBuilder<QuerySnapshot>(
    stream: Firestore.instance.collection('teams').snapshots(),
    builder: (BuildContext context,
        AsyncSnapshot<QuerySnapshot> snapshot) {
      if(snapshot.hasError){
        return new Text('Error: ${snapshot.error}');
      }
      switch(snapshot.connectionState){
        case ConnectionState.waiting: return new Text('Loading...');
        default:
          return new ListView(children: snapshot.data.documents
              .map((DocumentSnapshot document) {
            return Center(child: new Text(document['name']));
          }).toList());
      }
    },
  );
}
*/

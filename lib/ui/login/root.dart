
import 'package:f_app/ui/home/home.dart';
import 'package:f_app/ui/login/login.dart';
import 'package:flutter/material.dart';
import 'package:f_app/services/auth.dart';

class RootPage extends StatefulWidget {
     
   RootPage({this.auth});
   final Auth auth;
  @override
  State<StatefulWidget> createState() => _RootPageState();
}

enum AuthStatus {
  notSignedIn,
  signedIn,
}

class _RootPageState extends State<RootPage> {
    AuthStatus _authStatus =AuthStatus.notSignedIn;

    Map<String, dynamic> _profile;


   @override
   void initState() {
      super.initState();
      widget.auth.getCurrentUser().then((userId) {
         setState(() {
            _authStatus = userId == null ? AuthStatus.notSignedIn : AuthStatus.signedIn;
         });
      });

        widget.auth.profile.listen((state) => setState(() => _profile = state));
   }

   void _signedIn() {
      setState(() {
         _authStatus = AuthStatus.signedIn;
      });
   }

   void _signedOut() {
      setState(() {
         _authStatus = AuthStatus.notSignedIn;
      });
   }

   @override
   Widget build(BuildContext context) {
      switch(_authStatus) {
         case AuthStatus.notSignedIn:
            return Login(
               auth: widget.auth,
               onSignedIn: _signedIn,
            );
         case AuthStatus.signedIn:
              return HomePage(
              auth: widget.auth,
              onSignedOut: _signedOut,
              user: _profile
            );
      }

      return Login(auth: widget.auth);

   }

  // Widget _buildWaitingScreen() {
  //   return Scaffold(
  //     body: Container(
  //       alignment: Alignment.center,
  //       child: CircularProgressIndicator(),
  //     ),
  //   );
  // }

}